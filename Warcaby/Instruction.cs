﻿namespace Warcaby
{
    static class Instruction
    {
        //komunikat po kliknięciu przycisku "Instrukcja" (infoButton)
        public static string Text = "Kliknij na swojego pionka.\n"
            + "Następnie kliknij pole, na które ma przejść lub kliknij go ponownie, by wybrać inny pionek.\n"
            + "Pamiętaj, że gdy jest to możliwe, musisz zbijać pionki przeciwnika.\n"
            + "Gdy zbijesz wszystkie, wygrywasz!";
    }
}
