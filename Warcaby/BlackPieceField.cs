﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Warcaby
{
    //klasa zwykłego czarnego pionka
    class BlackPieceField : Field
    {
        public BlackPieceField(Button button) : base(button)
        {
            State = FieldState.Black;
            //wstawiamy obrazek pionka na polu - w wypadku problemów z odczytem pliku na polu pojawi się wersja tekstowa
            try
            {
                Image img = new Image();
                img.Source = new BitmapImage(new Uri("Images/blackpiece.png", UriKind.Relative));
                Element.Content = img;
            }
            catch
            {
                Element.Foreground = Brushes.Black;
                Element.Content = "O";
            }
            IsCorrect = false;
        }

        //ruch bez bicia - dla zwykłego pionka są maksymalnie 2 takie możliwe ruchy
        public override void CheckMove(int x, int y)
        {
            if (y < 7)
            {
                if (x < 7 && Game.Board[x + 1, y + 1].State == FieldState.Empty) Game.Board[x + 1, y + 1].Activate();
                if (x > 0 && Game.Board[x - 1, y + 1].State == FieldState.Empty) Game.Board[x - 1, y + 1].Activate();
            }
        }

        //tu znowu mamy maksymalnie 2 ruchy do sprawdzenia
        public override bool CheckBeat(int x, int y, bool enableTarget = true)
        {
            bool result = false;
            if (y < 6)
            {
                if (x > 1 && Game.Board[x - 1, y + 1].State == FieldState.White && Game.Board[x - 2, y + 2].State == FieldState.Empty)
                {
                    if (enableTarget) Game.Board[x - 2, y + 2].Activate();
                    result = true;
                }
                if (x < 6 && Game.Board[x + 1, y + 1].State == FieldState.White && Game.Board[x + 2, y + 2].State == FieldState.Empty)
                {
                    if (enableTarget) Game.Board[x + 2, y + 2].Activate();
                    result = true;
                }
            }
            return result;
        }
    }
}
