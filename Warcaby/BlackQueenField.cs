﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Warcaby
{
    //Klasa czarnej damki, zrobiona analogicznie do WhiteQueenField (która jednak ma więcej komentarzy)
    class BlackQueenField : Field
    {
        public BlackQueenField(Button button) : base(button)
        {
            State = FieldState.Black;
            try
            {
                Image img = new Image();
                img.Source = new BitmapImage(new Uri("Images/blackpieced.png", UriKind.Relative));
                Element.Content = img;
            }
            catch
            {
                Element.Foreground = Brushes.Black;
                Element.Content = "D";
            }
            IsCorrect = false;
        }

        public override void CheckMove(int x, int y)
        {
            int i = x;
            int j = y;
            while (i > 0 && j > 0)
            {
                if (Game.Board[i - 1, j - 1].State == FieldState.Empty) Game.Board[i - 1, j - 1].Activate();
                i--; j--;
            }

            i = x;
            j = y;
            while (i > 0 && j < 7)
            {
                if (Game.Board[i - 1, j + 1].State == FieldState.Empty) Game.Board[i - 1, j + 1].Activate();
                i--; j++;
            }

            i = x;
            j = y;
            while (i < 7 && j > 0)
            {
                if (Game.Board[i + 1, j - 1].State == FieldState.Empty) Game.Board[i + 1, j - 1].Activate();
                i++; j--;
            }

            i = x;
            j = y;
            while (i < 7 && j < 7)
            {
                if (Game.Board[i + 1, j + 1].State == FieldState.Empty) Game.Board[i + 1, j + 1].Activate();
                i++; j++;
            }
        }

        public override bool CheckBeat(int x, int y, bool enableTarget = true)
        {
            bool result = false;
            int i = x;
            int j = y;
            while (i > 0 && j > 0)
            {
                if (Game.Board[i, j].State == FieldState.White)
                {
                    if (Game.Board[i - 1, j - 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i - 1, j - 1].Activate();
                    }
                    break;
                }
                i--; j--;
            }

            i = x;
            j = y;
            while ((!result || enableTarget) && i > 0 && j < 7)
            {
                if (Game.Board[i, j].State == FieldState.White)
                {
                    if (Game.Board[i - 1, j + 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i - 1, j + 1].Activate();
                    }
                    break;
                }
                i--; j++;
            }

            i = x;
            j = y;
            while ((!result || enableTarget) && i < 7 && j > 0)
            {
                if (Game.Board[i, j].State == FieldState.White)
                {
                    if (Game.Board[i + 1, j - 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i + 1, j - 1].Activate();
                    }
                    break;
                }
                i++; j--;
            }

            i = x;
            j = y;
            while ((!result || enableTarget) && i < 7 && j < 7)
            {
                if (Game.Board[i, j].State == FieldState.White)
                {
                    if (Game.Board[i + 1, j + 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i + 1, j + 1].Activate();
                    }
                    break;
                }
                i++; j++;
            }
            return result;
        }
    }
}