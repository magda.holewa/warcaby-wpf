﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Warcaby
{
    //Przechowuje informacje o stanie pola i jego wyglądzie
    abstract class Field
    {
        //przycisk widoczny na ekranie
        public Button Element;
        //czy pole jest wolne czy zajęte (i przez którego gracza)
        public FieldState State;
        //czy jest prawidłowym wyborem w tej turze
        public bool IsCorrect;

        public Field(Button button)
        {
            //przypisuje polu właściwy element interfejsu graficznego
            Element = button;
            Element.BorderBrush = Brushes.Gray;
        }

        public void Activate()
        {
            //Pokazuje graczowi dozwolony cel
            IsCorrect = true;
            Element.BorderBrush = Brushes.Green;
        }

        //Wskazuje możliwe ruchy i aktywuje pola - cele ruchu (bez zbijania)
        public abstract void CheckMove(int x, int y);
        //Szuka możliwych ruchów z biciem (enableTarget okresla, czy aktywować graczowi pola-cele)
        //Zwraca, czy pionek na polu o współrzędnych (x, y) może bić w tej turze 
        public abstract bool CheckBeat(int x, int y, bool enableTarget = true);
    }
}
