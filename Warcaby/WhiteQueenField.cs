﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Warcaby
{
    //Klasa białej damki
    class WhiteQueenField : Field
    {
        public WhiteQueenField(Button button) : base(button)
        {
            State = FieldState.White;
            //dostosowujemy wygląd przycisku
            try
            {
                Image img = new Image();
                img.Source = new BitmapImage(new Uri("Images/whitepieced.png", UriKind.Relative));
                Element.Content = img;
            }
            //w wypadku błędu z odczytem pliku
            catch
            {
                Element.Foreground = Brushes.White;
                Element.Content = "D";
            }

            IsCorrect = false;
        }

        //Damka może się ruszać w 4 kierunkach o dowolną liczbę pól (ograniczoną tylko wielkością planszy lub pionkami przeciwnika)
        //obie metody sprawdzają mozliwości ruchów w tych 4 kierunkach (4 pętle), w każdej zaczynając od obecnego położenia pionka
        public override void CheckMove(int x, int y)
        {
            int i = x;
            int j = y;
            while (i > 0 && j > 0)
            {
                if (Game.Board[i - 1, j - 1].State == FieldState.Empty) Game.Board[i - 1, j - 1].Activate();
                i--; j--;
            }

            i = x;
            j = y;
            while (i > 0 && j < 7)
            {
                if (Game.Board[i - 1, j + 1].State == FieldState.Empty) Game.Board[i - 1, j + 1].Activate();
                i--; j++;
            }

            i = x;
            j = y;
            while (i < 7 && j > 0)
            {
                if (Game.Board[i + 1, j - 1].State == FieldState.Empty) Game.Board[i + 1, j - 1].Activate();
                i++; j--;
            }

            i = x;
            j = y;
            while (i < 7 && j < 7)
            {
                if (Game.Board[i + 1, j + 1].State == FieldState.Empty) Game.Board[i + 1, j + 1].Activate();
                i++; j++;
            }
        }

        public override bool CheckBeat(int x, int y, bool enableTarget = true)
        {
            bool result = false;
            int i = x;
            int j = y;
            while (i > 0 && j > 0)
            {
                if (Game.Board[i, j].State == FieldState.Black)
                {
                    if (Game.Board[i - 1, j - 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i - 1, j - 1].Activate();
                    }
                    break;
                }
                i--; j--;
            }
            i = x;
            j = y;
            //nie wchodzi w kolejne pętle, gdy już znaleniono możliwość bicia, a nas nie interesuje konkretny ruch
            //tylko sama opcja bicia na początku tury danego gracza (metoda Game.CheckAll())
            while ((!result || enableTarget) && i > 0 && j < 7)
            {
                if (Game.Board[i, j].State == FieldState.Black)
                {
                    if (Game.Board[i - 1, j + 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i - 1, j + 1].Activate();
                    }
                    break;
                }
                i--; j++;
            }

            i = x;
            j = y;
            while ((!result || enableTarget) && i < 7 && j > 0)
            {
                if (Game.Board[i, j].State == FieldState.Black)
                {
                    if (Game.Board[i + 1, j - 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i + 1, j - 1].Activate();
                    }
                    break;
                }
                i++; j--;
            }

            i = x;
            j = y;
            while ((!result || enableTarget) && i < 7 && j < 7)
            {
                if (Game.Board[i, j].State == FieldState.Black)
                {
                    if (Game.Board[i + 1, j + 1].State == FieldState.Empty)
                    {
                        result = true;
                        if (enableTarget) Game.Board[i + 1, j + 1].Activate();
                    }
                    break;
                }
                i++; j++;
            }
            return result;
        }
    }
}
