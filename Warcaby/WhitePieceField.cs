﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Warcaby
{
    //Klasa białego zwykłego pionka
    class WhitePieceField : Field
    {
        public WhitePieceField(Button button) : base(button)
        {
            State = FieldState.White;
            //wstawiamy obrazek pionka na polu - w wypadku problemów z odczytem pliku na polu pojawi się wersja tekstowa
            try
            {
                Image img = new Image();
                img.Source = new BitmapImage(new Uri("Images/whitepiece.png", UriKind.Relative));
                Element.Content = img;
            }
            catch
            {
                Element.Foreground = Brushes.White;
                Element.Content = "O";
            }
            //na początku gry białe pionki są aktywne
            //przy tworzeniu pola pionka później (po wykonaniu ruchu) wartość IsCorrect podana tutaj nie ma znaczenia
            //bo i tak zaraz potem aktywujemy inne pola
            IsCorrect = true;
        }

        //ruch bez bicia - dla zwykłego pionka są maksymalnie 2 takie możliwe ruchy
        public override void CheckMove(int x, int y)
        {
            if (y > 0)
            {
                if (x > 0 && Game.Board[x - 1, y - 1].State == FieldState.Empty) Game.Board[x - 1, y - 1].Activate();
                if (x < 7 && Game.Board[x + 1, y - 1].State == FieldState.Empty) Game.Board[x + 1, y - 1].Activate();
            }
        }

        //tu znowu mamy maksymalnie 2 ruchy do sprawdzenia
        public override bool CheckBeat(int x, int y, bool enableTarget = true)
        {
            bool result = false;
            if (y > 1)
            {
                if (x > 1 && Game.Board[x - 1, y - 1].State == FieldState.Black && Game.Board[x - 2, y - 2].State == FieldState.Empty)
                {
                    if (enableTarget) Game.Board[x - 2, y - 2].Activate();
                    result = true;
                }
                if (x < 6 && Game.Board[x + 1, y - 1].State == FieldState.Black && Game.Board[x + 2, y - 2].State == FieldState.Empty)
                {
                    if (enableTarget) Game.Board[x + 2, y - 2].Activate();
                    result = true;
                }
            }
            return result;
        }
    }
}