﻿using System;
using System.Windows.Media;

namespace Warcaby
{
    static class Game
    {
        //enum - stan gry
        public static GameState State;
        //wszystkie pola - są w tablicy, co pozwala na łatwe sprawdzanie i wykonywanie ruchów
        public static Field[,] Board;
        //treść komunikatu, który może pojawić się w oknie
        public static string LabelText;

        //pierwotne położenie pionka, który będzie się poruszał w danej turze
        private static int sourceX, sourceY;
        //czy gracz może bić (jakimkolwiek z pionków) - w warcabach mamy nakaz bicia, jeśli jest możliwe
        private static bool CanBeat;
        //ilość pionków graczy na polu
        private static int blackPieces, whitePieces;
        

        //początkowe ustawienia gry
        public static void Start()
        {
            State = GameState.White_Select_Piece;
            CanBeat = false;
            blackPieces = 12;
            whitePieces = 12;
            LabelText = "";
        }

        //wywoływane po kliknięciu pola
        public static void Proceed(int x, int y)
        {
            LabelText = "";
            //wyświetla komunikat gdy gracz wybierze niewłaściwe pole
            if (!Board[x, y].IsCorrect) throw new ArgumentException();
            //w innym wypadku wykonuje odpowiednią akcję zależną od momentu gry
            switch (State)
            {
                case GameState.White_Select_Piece:
                    SelectPiece(x, y);
                    break;
                case GameState.White_Select_Target:
                    SelectTarget(x, y, FieldState.White, FieldState.Black, ref blackPieces);
                    break;
                case GameState.Black_Select_Piece:
                    SelectPiece(x, y);
                    break;
                case GameState.Black_Select_Target:
                    SelectTarget(x, y, FieldState.Black, FieldState.White, ref whitePieces);
                    break;
                default:
                    break;
            }
        }

        private static void SelectPiece(int x, int y)
        {
            DisableAll();
            //jeśli gracz może bić, wskazuje możliwe bicia i aktywuje pola
            if (CanBeat) Board[x, y].CheckBeat(x, y);
            //jeśli nie może, szuka ruchów bez bicia
            else Board[x, y].CheckMove(x, y);
            //pole z pionkiem jest aktywne, by móc wrócić do wyboru pionka
            //np. w wypadku braku możliwości ruchu tym pionkiem
            Board[x, y].Activate();
            //zapisujemy, skąd pionek się rusza
            sourceX = x; sourceY = y;
            //zmienia stan na ..._Select_Target
            State++;
        }

        private static void SelectTarget(int x, int y, FieldState playerState, FieldState opponentState, ref int opponentPieces)
        {
            if (x == sourceX && y == sourceY)
            {
                //powrót do wyboru pionka
                Enable(playerState);
            }
            else
            {
                //wykonanie ruchu - "zamiana" pustego pola z polem pionka
                Swap(x, y);
                //Sign przyjmuje tu wartość -1 lub 1 co pozwala nam określić kierunek ruchu pionka
                //i położenie pionka zbitego przez niego
                if (Board[x + Math.Sign(sourceX - x), y + Math.Sign(sourceY - y)].State == opponentState)
                {
                    //usuwamy zbity pionek przeciwnika
                    Kill(x + Math.Sign(sourceX - x), y + Math.Sign(sourceY - y));
                    opponentPieces--;
                    //brak wrogich pionków - wygrana
                    if (opponentPieces == 0)
                    {
                        State = GameState.Win;
                        LabelText = "Wygrana!\nMożesz zacząć\nnową grę";
                    }
                    //jeśli znajdą się kolejne możliwości zbicia, metoda CheckBeat aktywuje je i zwróci true
                    else if (Board[x, y].CheckBeat(x, y))
                    {
                        sourceX = x;
                        sourceY = y;
                        LabelText = "Możesz bić\njeszcze raz!";
                    }
                    //nie jest możliwe kolejne bicie - zaczyna się tura przeciwnika
                    else
                    {
                        Enable(opponentState);
                        CanBeat = CheckAll(opponentState);
                    }
                }
                //tak samo, jeśli żadnego bicia nie było
                else
                {
                    Enable(opponentState);
                    CanBeat = CheckAll(opponentState);
                }
            }
        }

        //zamienia typy dwóch pól: tego, z którego pionek się ruszał i tego, na które się przeniósł
        private static void Swap(int i, int j)
        {
            //sprawdzamy, jaki pionek przesunął się na dane pole
            if (Board[sourceX, sourceY].State == FieldState.White)
            {
                //"przenosi" istniejącą damkę lub tworzy damkę, gdy pionek dociera na drugi koniec planszy
                if (j == 0 || Board[sourceX, sourceY].GetType() == typeof(WhiteQueenField))
                    Board[i, j] = new WhiteQueenField(Board[i, j].Element);
                else Board[i, j] = new WhitePieceField(Board[i, j].Element);
            }
            //to samo dla czarnych pionków
            else
            {
                if (j == 7 || Board[sourceX, sourceY].GetType() == typeof(BlackQueenField))
                    Board[i, j] = new BlackQueenField(Board[i, j].Element);
                else Board[i, j] = new BlackPieceField(Board[i, j].Element);
            }
            //pole, na którym był wcześniej pionek robi się puste
            Kill(sourceX, sourceY);
        }

        //usuwa pionka z danego pola
        private static void Kill(int i, int j)
        {
            Board[i, j] = new EmptyField(Board[i, j].Element);
        }

        //dezaktywuje wszystkie pola
        private static void DisableAll()
        {
            foreach (var field in Board)
            {
                field.IsCorrect = false;
            }
        }
        
        private static void Enable(FieldState turn)
        {
            if (turn != FieldState.Empty)
            {
                foreach (var field in Board)
                {
                    if (field.IsCorrect)
                    {
                        field.IsCorrect = false;
                        //usuwa zieloną ramkę która oznaczała możliwe ruchy
                        field.Element.BorderBrush = Brushes.Gray;
                    }
                    //aktywuje pola z pionkami danego gracza
                    if (field.State == turn) field.IsCorrect = true;
                }
                //ustawia stan gry
                if (turn == FieldState.White) State = GameState.White_Select_Piece;
                else State = GameState.Black_Select_Piece;
            }
        }

        //sprawdza, czy gracz może być którymś z pionków, ale nie aktywuje żadnych pól
        //(wywołujemy CheckBeat z enableTarget = false)
        //potrzebna jest tylko informacja, czy nakazywać bicie w tej turze
        private static bool CheckAll(FieldState turn)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (Board[i, j].State == turn && Board[i, j].CheckBeat(i, j, false)) return true;
                }
            }
            return false;
        }
    }
}
