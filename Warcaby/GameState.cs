﻿namespace Warcaby
{
    enum GameState
    {
        White_Select_Piece,     //wybór pionka
        White_Select_Target,    //wybór celu ruchu pionka
        Black_Select_Piece,
        Black_Select_Target,
        Win
    }
}
