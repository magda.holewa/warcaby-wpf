﻿using System.Windows.Controls;

namespace Warcaby
{
    //Przechowuje informacje o polu, na którym nie znajduje się żaden pionek
    class EmptyField : Field
    {
        public EmptyField(Button button) : base(button)
        {
            State = FieldState.Empty;
            Element.Content = "";
            IsCorrect = false;
        }

        public override void CheckMove(int x, int y)
        {
            //Metoda Move musi się tu znaleźć, ale z pustego pola nie ma się co ruszać
        }

        public override bool CheckBeat(int x, int y, bool enableTarget)
        {
            return false;
        }
    }
}
