﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Warcaby
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //po włączeniu od razu zaczynamy nową grę
            ClearArray();
            Game.Start();
        }

        //tworzy planszę i pola wykorzystujące przyciski - elementy tego okna
        //do takich pól i przycisków możemy się odwołać z każdego miejsca w kodzie
        private void ClearArray()
        {
            Game.Board = new Field[8, 8]
            {
                {new EmptyField(A1), new BlackPieceField(A2), new EmptyField(A3), new EmptyField(A4),
                    new EmptyField(A5), new WhitePieceField(A6), new EmptyField(A7), new WhitePieceField(A8)},
                {new BlackPieceField(B1), new EmptyField(B2), new BlackPieceField(B3), new EmptyField(B4),
                    new EmptyField(B5), new EmptyField(B6), new WhitePieceField(B7), new EmptyField(B8)},
                {new EmptyField(C1), new BlackPieceField(C2), new EmptyField(C3), new EmptyField(C4),
                    new EmptyField(C5), new WhitePieceField(C6), new EmptyField(C7), new WhitePieceField(C8)},
                {new BlackPieceField(D1), new EmptyField(D2), new BlackPieceField(D3), new EmptyField(D4),
                    new EmptyField(D5), new EmptyField(D6), new WhitePieceField(D7), new EmptyField(D8)},
                {new EmptyField(E1), new BlackPieceField(E2), new EmptyField(E3), new EmptyField(E4),
                    new EmptyField(E5), new WhitePieceField(E6), new EmptyField(E7), new WhitePieceField(E8)},
                {new BlackPieceField(F1), new EmptyField(F2), new BlackPieceField(F3), new EmptyField(F4),
                    new EmptyField(F5), new EmptyField(F6), new WhitePieceField(F7), new EmptyField(F8)},
                {new EmptyField(G1), new BlackPieceField(G2), new EmptyField(G3), new EmptyField(G4),
                    new EmptyField(G5), new WhitePieceField(G6), new EmptyField(G7), new WhitePieceField(G8)},
                {new BlackPieceField(H1), new EmptyField(H2), new BlackPieceField(H3), new EmptyField(H4),
                    new EmptyField(H5), new EmptyField(H6), new WhitePieceField(H7), new EmptyField(H8)},
            };
        }

        //wywołuje się przy kliknięciu czarnego pola (z białymi w warcabach nic się nie dzieje)
        private void field_Click(object sender, RoutedEventArgs e)
        {
            //oblicza pozycję (indeks w tablicy) pola na planszy na podstawie pozycji przycisku w oknie
            int x = (Convert.ToInt32(((Button)sender).Margin.Left) - 10) / 50;
            int y = (Convert.ToInt32(((Button)sender).Margin.Top) - 10) / 50;

            try
            {
                Game.Proceed(x, y);
            }
            //rzucony gdy gracz wybierze nieprawidłowy ruch
            catch (ArgumentException argEx)
            {
                Game.LabelText = "Zły przycisk!\nSpróbuj jeszcze\nraz";
            }
            //jakikolwiek nieprzewidziany wyjątek
            catch (Exception ex)
            {
                MessageBox.Show("Ktoś coś zepsuł (wcale nie sugeruję, że ty)... Radzę zacząć grę od nowa.");
            }
            //wyświetla odpowiednie komunikaty obok planszy
            SetLabelText();
        }

        private void SetLabelText()
        {
            messageLabel.Content = Game.LabelText;
            //zmienia treść, gdy zaczyna się nowa tura danego gracza
            if (Game.State == GameState.White_Select_Piece) playerLabel.Content = "Białe";
            else if (Game.State == GameState.Black_Select_Piece) playerLabel.Content = "Czarne";
        }

        private void newGameButton_Click(object sender, RoutedEventArgs e)
        {
            ClearArray();
            Game.Start();
            playerLabel.Content = "Białe";
        }

        private void infoButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(Instruction.Text);
        }
    }
}
